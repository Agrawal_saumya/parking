#### Getting started:

#### Prerequisites:

#### 1. Python 3.5 or above
	*To install python download from the given link.
	*Windows :https://www.python.org/downloads/windows/
	*Mac:https://www.python.org/downloads/mac-osx/
	*Others :https://www.python.org/download/other/

#### 2. Packages to be installed
	*Package - configparser
	*For Guide how to install a library in python, refer below guide
	*https://packaging.python.org/tutorials/installing-packages/

#### 3. Configurations required
	*Store the given code at a location in your desktop. Open ./Parking/Config/config.ini file. 
	*Under location key, fill the location where you have saved the project. For example, if my folder Parking is stored at D:\Project, fill the same 'D:\Project' in location
	*Place you input commands file at './Parking/Data/Input', as filename.txt, after code run you will find the output file named Output_filename.txt at location './Parking/Data/Output'

#### 4. Open cmd prompt from Parking folder and run following commands:
	*python
	*from Parking import Parking
	*P=Parking('your file name')



