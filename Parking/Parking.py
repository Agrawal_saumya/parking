"""
This is the runner class. It accepts a  file name as input, whose file has commands to be run and creates an output file
following the commands. The input file is expected at './Parking/Data/Input' location and the file generates output at
'./Parking/Data/Output' location.
The functions corresponding to each commands have been stored in a file 'ParkingCommands.txt'
located at './Parking/Config' location

---Created by Saumya Agrawal-----
"""

import json
from Code.Execute import Execute
from Code.configRead import returnLocation


class Parking():
    def __init__(self,filename):
        try:
            self.location = returnLocation()
            self.filename=filename
            location=self.location+'\\Parking\\Data\\Input\\'
            inputFile = open(location+filename, 'r')
            listCommands = inputFile.readlines()
            self.runCommands(listCommands)
            print('Success')
        except Exception as e:
            print(e)

    def runCommands(self,inputs):
        with open(self.location+'\\Parking\\Config\\ParkingCommands.txt') as data:
            parkingCommands = json.load(data)
        E = Execute()
        for input in inputs:
            command=input.split(' ')[0]
            getattr(Execute,parkingCommands[command])(E,command,input)
        data.close()
        outputFile = open(self.location+'\\Parking\\Data\\Output\\Output_'+self.filename, "w")
        for item in E.returnCommands:
            outputFile.write("%s\n" % item)
        outputFile.close()










if __name__ == '__main__':
    P=Parking('input.txt')