"""
This class creates a vehicle Object and stores information related to vehicle.
These information includes vehicle registration number, driver's age and slot provided to the car.
This class also has member methods that return these information when called

---Created by Saumya Agrawal-----
"""

class VehicleDetails():
    slotNumber=0
    def __init__(self,vehicleRegistrationNumber,ageDriver):
        self.vehicleRegistrationNumber=vehicleRegistrationNumber
        self.ageDriver=ageDriver
        self.slotNumber=0

    def assignSlot(self,slotNumber):
        self.slotNumber=slotNumber

    def getslotNumber(self):
        return self.slotNumber

    def getageDriver(self):
        return self.ageDriver

    def getvehicleRegistrationNumber(self):
        return self.vehicleRegistrationNumber
