import configparser

test_config = configparser.ConfigParser()
test_config._interpolation = configparser.ExtendedInterpolation()
test_config.optionxform = str
test_config.read('./Config/config.ini')

def returnLocation():
    location=test_config['Details']['location']
    return location

if __name__ == '__main__':
    returnLocation()
