"""
This is the class that performs each command and add the output result to the output file.
It has member methods for each command. The syntax for output results are fetched from a metadata file 'OutputStatements.txt'
located at './Parking/Config' location.

---Created by Saumya Agrawal-----
"""

import json
from Code.VehicleDetails import VehicleDetails
from Code.configRead import returnLocation

class Execute():
    def __init__(self):
        self.returnCommands=[]
        self.slots=0
        self.allVehicles=[]


    def createParkingLot(self,command,commandLine):
        #Command example : Create_parking_lot 6
        #Output example : Created parking of 6 slots
        self.slots=int(commandLine.split(' ')[1].strip())
        if self.slots>0 and self.slots<=1000:
            self.returnCommands.append(self.buildOutput(command,
                                                    {
                                                        "slot":self.slots
                                                    }))
        else:
            self.returnCommands.append(self.buildOutput('slotRange', {}))


    def parkTheVehicle(self,command,commandLine):
        # Command example : Park KA-01-HH-1234 driver_age 21
        # Output example : Car with vehicle registration number "KA-01-HH-1234" has been parked at slot number 1
        self.VRN = commandLine.split(' ')[1].strip()
        self.age=commandLine.split(' ')[3].strip()
        C = VehicleDetails(self.VRN,self.age)
        slot = self.getSlot(C)
        if slot!=-1:
            C.assignSlot(slot)
            self.returnCommands.append(self.buildOutput(command,
                                                    {
                                                        "VRN": self.VRN,
                                                        "slot":slot
                                                    }))
        else:
            self.returnCommands.append(self.buildOutput('CouldNotPark',
                                                        {
                                                            "VRN": self.VRN
                                                        }))

    def returnSlotNumberForAge(self,command,commandLine):
        # Command example : Slot_numbers_for_driver_of_age 21
        # Output example : 1,2
        listDrivers=[]
        age=commandLine.split(' ')[1].strip()
        for car in self.allVehicles:
            if car.getageDriver()==age:
                listDrivers.append(str(car.getslotNumber()))
        if listDrivers!=[]:
            self.returnCommands.append(self.buildOutput(command,
                                                    {
                                                        "slots": ','.join(listDrivers)
                                                    }))
        else:
            self.returnCommands.append(self.buildOutput('notFound',{}))


    def returnSlotNumberForCar(self,command,commandLine):
        # Command example : Slot_number_for_car_with_number PB-01-HH-1234
        # Output example : 2
        listDrivers = ''
        VRN = commandLine.split(' ')[1].strip()
        for car in self.allVehicles:
            if car.getvehicleRegistrationNumber() == VRN:
                listDrivers=car.getslotNumber()
        if listDrivers!='':
            self.returnCommands.append(self.buildOutput(command,
                                                    {
                                                        "slot": listDrivers
                                                    }))
        else:
            self.returnCommands.append(self.buildOutput('notFound', {}))

    def leaveParkingLot(self,command,commandLine):
        # Command example : Leave 2
        # Output example : Slot number 2 vacated, the car with vehicle registration number "PB-01-HH-1234" left the space, the driver of the car was of age 21
        slot=int(commandLine.split(' ')[1].strip())
        if len(self.allVehicles)>=slot:
            car=self.allVehicles[slot-1]
            self.returnCommands.append(self.buildOutput(command,
                                                        {
                                                            "slotNumber": car.getslotNumber(),
                                                            "VRN": car.getvehicleRegistrationNumber(),
                                                            "age": car.getageDriver()
                                                        }))
            self.allVehicles.pop(slot-1)
        else:
            self.returnCommands.append(self.buildOutput('emptySlot', {
                                                            "slot":slot
            }))


    def returnDriversForRegistrationNumber(self,command,commandLine):
        # Command example : Vehicle_registration_number_for_driver_of_age 18
        # Output example : Car with vehicle registration number "HR-29-TG-3098" has been parked at slot number 2
        listVRN=[]
        age=commandLine.split(' ')[1].strip()
        for car in self.allVehicles:
            if car.getageDriver() == age:
                listVRN.append(car.getvehicleRegistrationNumber())
        if listVRN!=[]:
            self.returnCommands.append(self.buildOutput(command,
                                                        {
                                                            "VRN": ','.join(listVRN)
                                                        }))
        else:
            self.returnCommands.append(self.buildOutput('notFound', {}))

    def getSlot(self,Car):
        # This function tries to provide slot to a vehicle it is possible as per parking space
        if len(self.allVehicles)<int(self.slots):
            for i in range(1,len(self.allVehicles)+1):
                if i!=self.allVehicles[i-1].getslotNumber():
                    self.allVehicles.insert(i-1, Car)
                    return i
            self.allVehicles.insert(len(self.allVehicles), Car)
            return len(self.allVehicles)
        else:
            return -1

    def buildOutput(self, keyName, parametersDictionary):
        with open(returnLocation()+'\\Parking\\Config\\OutputStatements.txt') as data:
            self.parkingCommands = json.load(data)
        specificOutput = self.parkingCommands[keyName]
        formattedOutput = specificOutput.format(**parametersDictionary)
        return formattedOutput
